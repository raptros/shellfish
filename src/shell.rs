use std::collections::HashMap;
use std::fmt::Display;
use std::io;
use std::io::prelude::*;

#[cfg(feature = "rustyline")]
use rustyline::{error::ReadlineError, Editor};
use thiserror::Error;
use yansi::Paint;

use crate::*;

/// A shell represents a shell for editing commands in.
///
/// Here are the generics:
///  * T: The state.
///  * M: The prompt. Can be anything that can be printed.
///  * H: The handler. Should implement either `Handler` or `AsyncHandler`, or
///    no functionality is present.
#[derive(Clone)]
pub struct Shell<'a, T, M: Display, H> {
    /// The shell prompt.
    ///
    /// It can be anything which implements Display and can therefore be
    /// printed (This allows for prompts that change with the state.)
    pub prompt: M,
    /// This is a list of commands for the shell. The hashmap key is the
    /// name of the command (ie `"greet"`) and the value is a wrapper
    /// to the function it corresponds to (as well as help information.)
    pub commands: HashMap<&'a str, Command<T>>,
    /// This is the state of the shell. This stores any values that you
    /// need to be persisted over multiple shell commands. For example
    /// it may be a simple counter or maybe a session ID.
    pub state: T,
    /// This is the handler for commands. See the [`Handler`](crate::Handler)
    /// documentation for more.
    pub handler: H,
    /// This is the description of the shell as a whole. This is displayed when
    /// requesting help information.
    pub description: String,
}

impl<'a, T, M: Display> Shell<'a, T, M, handler::DefaultHandler> {
    /// Creates a new shell
    pub fn new(state: T, prompt: M) -> Self {
        Shell {
            prompt,
            commands: HashMap::new(),
            state,
            handler: handler::DefaultHandler(),
            description: String::new(),
        }
    }
}

#[cfg(feature = "async")]
#[cfg_attr(nightly, doc(cfg(feature = "async")))]
impl<'a, T, M: Display> Shell<'a, T, M, handler::DefaultAsyncHandler> {
    /// Creates a new shell
    pub fn new_async(state: T, prompt: M) -> Self {
        Shell {
            prompt,
            commands: HashMap::new(),
            state,
            handler: handler::DefaultAsyncHandler(),
            description: String::new(),
        }
    }
}

impl<'a, T, M: Display, H: Handler<T>> Shell<'a, T, M, H> {
    /// Creates a new shell with the given handler.
    pub fn new_with_handler(state: T, prompt: M, handler: H) -> Self {
        Shell {
            prompt,
            commands: HashMap::new(),
            state,
            handler,
            description: String::new(),
        }
    }

    /// Starts running the shell
    pub fn run(&mut self) -> io::Result<()> {
        // Get the stdin & stdout.
        #[cfg(not(feature = "rustyline"))]
        let stdin = io::stdin();
        #[cfg(feature = "rustyline")]
        let mut rl = Editor::<()>::new().map_err(convert_rustyline_to_io)?;
        let mut stdout = io::stdout();

        '_shell: loop {
            // Display the prompt
            print!("{}", self.prompt);
            stdout.flush()?;

            // Read a line
            #[cfg(not(feature = "rustyline"))]
            let mut line = String::new();
            #[cfg(feature = "rustyline")]
            let line;

            #[cfg(not(feature = "rustyline"))]
            {
                stdin.read_line(&mut line)?;
            }
            #[cfg(feature = "rustyline")]
            {
                let readline = rl.readline(&self.prompt.to_string());
                match readline {
                    Ok(rl_line) => {
                        rl.add_history_entry(&rl_line);
                        line = rl_line;
                    }
                    Err(ReadlineError::Interrupted) => continue '_shell,
                    Err(ReadlineError::Eof) => break '_shell,
                    Err(err) => return Err(convert_rustyline_to_io(err)),
                }
            }

            // Runs the line
            match Self::unescape(line.trim()) {
                Ok(line) => {
                    if self.handler.handle(
                        line,
                        &self.commands,
                        &mut self.state,
                        &self.description,
                    ) {
                        break '_shell;
                    }
                }
                Err(e) => eprintln!("{}", Paint::red(e.to_string().as_str())),
            }
        }
        Ok(())
    }
}

#[cfg(feature = "async")]
#[cfg_attr(nightly, doc(cfg(feature = "async")))]
impl<'a, T: Send, M: Display, H: AsyncHandler<T>> Shell<'a, T, M, H> {
    /// Creates a new shell with the given handler.
    pub fn new_with_async_handler(state: T, prompt: M, handler: H) -> Self {
        Shell {
            prompt,
            commands: HashMap::new(),
            state,
            handler,
            description: String::new(),
        }
    }

    /// Starts running the shell
    pub async fn run_async(&mut self) -> io::Result<()> {
        // Get the stdin & stdout.
        #[cfg(not(feature = "readline"))]
        cfg_if::cfg_if! {
            if #[cfg(features = "async_std")] {
                let stdin = async_std::io::stdin();
            } else if #[cfg(features = "tokio")] {
                let stdin = tokio::io::stdin();
            } else {
                let stdin = io::stdin();
            }
        }

        #[cfg(feature = "rustyline")]
        let mut rl = Editor::<()>::new().map_err(convert_rustyline_to_io)?;

        cfg_if::cfg_if! {
            if #[cfg(features = "async_std")] {
                let mut stdout = async_std::io::stdout();
            } else {
                let mut stdout = io::stdout();
            }
        }

        '_shell: loop {
            // Display the prompt
            print!("{}", self.prompt);
            cfg_if::cfg_if! {
                if #[cfg(features = "tokio")] {
                    tokio::spawn(async { stdout.flush().await? })?;
                } else if #[cfg(feature = "async_std")] {
                    async_std::task::spawn(async { stdout.flush().await? })?;
                } else {
                    stdout.flush()?;
                }
            }
            // Read a line
            #[cfg(not(feature = "rustyline"))]
            let mut line = String::new();
            #[cfg(feature = "rustyline")]
            let line;

            #[cfg(not(feature = "rustyline"))]
            cfg_if::cfg_if! {
                if #[cfg(feature = "async_std")] {
                    stdin.read_line(&mut line).await?;
                } else {
                    stdin.read_line(&mut line)?;
                }
            }
            #[cfg(feature = "rustyline")]
            {
                let readline = rl.readline(&self.prompt.to_string());
                match readline {
                    Ok(rl_line) => {
                        rl.add_history_entry(&rl_line);
                        line = rl_line;
                    }
                    Err(ReadlineError::Interrupted) => continue '_shell,
                    Err(ReadlineError::Eof) => break '_shell,
                    Err(err) => panic!(),
                }
            }

            // Runs the line
            match Self::unescape(line.trim()) {
                Ok(line) => {
                    if self
                        .handler
                        .handle_async(
                            line,
                            &self.commands,
                            &mut self.state,
                            &self.description,
                        )
                        .await
                    {
                        break '_shell;
                    }
                }
                Err(e) => eprintln!("{}", Paint::red(e.to_string())),
            }
        }
        Ok(())
    }
}

#[derive(Error, Debug)]
pub enum UnescapeError {
    #[error("unhandled escape sequence \\{0}")]
    UnhandledEscapeSequence(char),
    #[error("unclosed quotes")]
    UnclosedQuotes,
}

impl<'a, T, M: Display, H> Shell<'a, T, M, H> {
    /// Unescapes a line and gets the arguments.
    fn unescape(command: &str) -> Result<Vec<String>, UnescapeError> {
        // Create a vec to store the split int.
        let mut vec = vec![String::new()];

        // Are we in an escape sequence?
        let mut escape = false;

        // Are we in a string?
        let mut string = false;

        // Go through each char in the string
        for c in command.chars() {
            let segment = vec.last_mut().unwrap();
            if escape {
                match c {
                    '\\' => segment.push('\\'),
                    ' ' if !string => segment.push(' '),
                    'n' => segment.push('\n'),
                    'r' => segment.push('\r'),
                    't' => segment.push('\t'),
                    '"' => segment.push('"'),
                    _ => return Err(UnescapeError::UnhandledEscapeSequence(c)),
                }
                escape = false;
            } else {
                match c {
                    '\\' => escape = true,
                    '"' => string = !string,
                    ' ' if string => segment.push(c),
                    ' ' if !string => vec.push(String::new()),
                    _ => segment.push(c),
                }
            }
        }

        if string {
            return Err(UnescapeError::UnclosedQuotes);
        }

        if vec.len() == 1 && vec[0].is_empty() {
            vec.clear();
        }

        Ok(vec)
    }
}

#[cfg(feature = "rustyline")]
fn convert_rustyline_to_io(e: ReadlineError) -> io::Error {
    match e {
        ReadlineError::Io(e) => e,
        ReadlineError::Errno(e) => e.into(),
        e => io::Error::new(io::ErrorKind::Interrupted, e),
    }
}
